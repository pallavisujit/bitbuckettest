package collectionTutorials;
import java.util.*;

public class ArrayListTutorial {
	
	
	static ArrayList<String> arrObj = new  ArrayList<String>();
	
	public static void addElementInList()
	{

		arrObj.add("A");
		arrObj.add("B");
		arrObj.add("C");
		arrObj.add("D");
		arrObj.add("E");
		
	}
	
	public static void printElementOfArray()
	{
		Iterator itr = arrObj.iterator();
		while(itr.hasNext())
		{
			Object element = itr.next();
			System.out.println("Element in array at position : "+element);
			
		}
		
		
		
	}
	public static void removeElementByName(int position)
	{
		arrObj.remove(position);
	}
	
public static void main(String args[])
{
	addElementInList();
	printElementOfArray();
	removeElementByName(3);
	System.out.println("Elements in array after removeing element :");
	printElementOfArray();
	//write a program for sort arrayList in order
	
}
}
